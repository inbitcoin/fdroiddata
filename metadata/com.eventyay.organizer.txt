AntiFeatures:Tracking
Categories:Internet,Connectivity
License:GPL-3.0-only
Web Site:https://eventyay.com/
Source Code:https://github.com/fossasia/open-event-orga-app
Issue Tracker:https://github.com/fossasia/open-event-orga-app/issues
Changelog:https://github.com/fossasia/open-event-orga-app/releases

Auto Name:${appName}
Summary:Event management app for Organizers using eventyay platform
Description:
Eventyay Organizer App provides event management features like:

* Event manager log in and sign up
* Manual attendee check in
* Attendee check in through QR code scan
* Basic event analytics
* Basic ticket management
* Event Detail
* Organizer Detail
.

Repo Type:git
Repo:https://github.com/fossasia/open-event-orga-app

Build:1.0.6alpha,7
    commit=v1.0.6alpha
    subdir=app
    gradle=fdroid

Build:1.1.0,8
    commit=v1.1.0
    subdir=app
    gradle=fdroid

Build:1.1.1,9
    commit=v1.1.1
    subdir=app
    gradle=fdroid

Build:1.2.1,12
    commit=v1.2.1
    subdir=app
    gradle=fdroid

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.1
Current Version Code:12
